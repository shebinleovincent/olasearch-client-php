# Ola Search API Client for PHP

[Ola Search](https://www.olasearch.com) is an AI-powered platform to build smart search-based apps and chatbots. We help to build and maintain great experiences when exploring and discovering information or finding answers.

The **Ola Search API Client for PHP** lets
you easily use the [Ola Search REST API](https://www.olasearch.com/) from
your PHP code.

[![Software License](https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square)](LICENSE) [![Latest Stable Version](https://poser.pugx.org/shebinleovincent/olasearch-client-php/v/stable.svg)](https://packagist.org/packages/shebinleovincent/olasearch-client-php)


If you're a Laravel user, you're probably looking for the following integrations:

- **Laravel**: [Ola Search Laravel Scout](https://gitlab.com/shebinleovincent/olasearch-laravel-scout)



## API Documentation

You can find the full reference on [Ola Search's website](https://olasearch.com/).



1. **[Supported platforms](#supported-platforms)**


2. **[Install](#install)**


3. **[Get started](#get-started)**


4. **[Index data](#index-data)**


5. **[Search](#search)**


6. **[Search UI](#search-ui)**


# Getting Started



## Supported platforms

This library is compatible with PHP version 5.3 and later.

## Install

### With composer (Recommended)

Install the package via [Composer](https://getcomposer.org/doc/00-intro.md):

```bash
composer require shebinleovincent/olasearch-client-php
```

### Without composer

If you don't use Composer, you can download the [package](https://gitlab.com/shebinleovincent/olasearch-client-php/-/archive/master/olasearch-client-php-master.zip) and include it in your code.

```php
require_once('olasearch-client-php/olasearch.php');
```

### Framework Integrations

We officially provide support for the **Laravel** framework:

If you are using laravel framework, have a look at our
[Laravel documentation](https://olasearch.com/)

## Get started

Below instructions will show you how to index and search documents.

### Initialize the client

To begin, you will need to initialize the client. In order to do this you will need your **Project ID** and **API Key**.
You can find both on [your Ola Search account](https://admin.olasearch.com/login).

```php
// composer autoload
require __DIR__ . '/vendor/autoload.php';

// if you are not using composer
// require_once 'path/to/olasearch.php';

$client = new \OlaSearch\Client('YourProjectID', 'YourAdminAPIKey');

$index = $client->initIndex();
```

## Index data

Following code shows how to index documents:

```php
$index = $client->initIndex();
$documents = json_decode(file_get_contents('articles.json'), true);
$mapping = json_decode(file_get_contents('mapping.json'), true);
$index->addDocuments($documents, $mapping);
```

## Search

You can now search for articles using `title`, `category`, `author`, etc.:

```php
// search by title
var_dump($index->search('falcon heavy'));

// search articles by author
var_dump($index->search('articles by jackie wattles'));

// search articles by category
var_dump($index->search('articles in technology'));
```

## Search UI

**Note:** If you are building a web application, you may be more interested in using one of our
[frontend search UI libraries](https://admin.olasearch.com/projects/)

### Web chatbot
Embed chatbot on your website

Place the following code where you'd like chatbot to appear
```html
<div id="ola-chatbot"></div>
<link rel='stylesheet' href="https://cdn.olasearch.com/assets/css/olasearch.core.min.css" type='text/css'>
<script src="https://cdn.olasearch.com/production/<YourProjectID>/olasearch.min.js"></script>
```

### Autocomplete search bar
Embed autocomplete search bar results page

Place the following code where you'd like autocomplete search bar to appear
```html
<div id="ola-autosuggest"></div>
<link rel='stylesheet' href="https://cdn.olasearch.com/assets/css/olasearch.core.min.css" type='text/css'>
<script src="https://cdn.olasearch.com/production/<YourProjectID>/olasearch.min.js"></script>
```

### Search results
Embed search results page

Place the following code where you'd like search results page to appear
```html
<div id="ola-serp"></div>
<link rel='stylesheet' href="https://cdn.olasearch.com/assets/css/olasearch.core.min.css" type='text/css'>
<script src="https://cdn.olasearch.com/production/<YourProjectID>/olasearch.min.js"></script>
```


