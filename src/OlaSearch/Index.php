<?php

/*
 * Copyright (c) 2018 Ola Search
 * http://www.olasearch.com/
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 *
 */

namespace OlaSearch;


/*
 * Contains all the functions related to indexing
 * You should use Client.initIndex() to retrieve this object
 */

class Index {
	/**
	 * @var ClientContext
	 */
	private $context;

	/**
	 * @var Client
	 */
	private $client;

	/**
	 * Index initialization (You should not instantiate this yourself).
	 *
	 * @param ClientContext $context
	 * @param Client $client
	 *
	 * @internal
	 */
	public function __construct( ClientContext $context, Client $client ) {
		$this->context = $context;
		$this->client  = $client;
	}

	/**
	 * Add several documents.
	 *
	 * @param array $documents contains an array of documents to add.
	 * @param array $mapping
	 *
	 * @return mixed
	 * @throws OlaSearchException
	 */
	public function addDocuments( $documents, $mapping = array() ) {

		$requestHeaders = array();
		$nbArgs         = func_num_args();
		if ( $nbArgs > 2 ) {
			$requestHeaders = is_array( func_get_arg( $nbArgs - 1 ) ) ? func_get_arg( $nbArgs - 1 ) : array();
		}

		$data = array( 'mapping' => $mapping, 'docs' => $documents, );
		return $this->client->request(
			$this->context,
			'POST',
			'/connector/document',
			array(),
			$data,
			$this->context->writeHostsArray,
			$this->context->connectTimeout,
			$this->context->readTimeout,
			$requestHeaders
		);
	}

	/**
	 * Delete several documents.
	 *
	 * @param array $documentIDs contains an array of documentIDs to delete. If the document contains an documentID
	 *
	 * @return mixed
	 * @throws OlaSearchException
	 */
	public function deleteDocuments( $documentIDs ) {
		$requestHeaders = func_num_args() === 2 && is_array( func_get_arg( 1 ) ) ? func_get_arg( 1 ) : array();

		return $this->client->request(
			$this->context,
			'DELETE',
			'/connector/document',
			null,
			$documentIDs,
			$this->context->writeHostsArray,
			$this->context->connectTimeout,
			$this->context->readTimeout,
			$requestHeaders
		);
	}

	/**
	 * Search inside the index.
	 *
	 * @param string $query the full text query
	 * @param mixed $searchParameters (optional) if set, contains an associative array with query parameters:
	 * @param array $requestHeaders
	 *
	 * @return mixed
	 * @throws OlaSearchException
	 */
	public function search( $query, $searchParameters = null ) {
		$requestHeaders = func_num_args() === 3 && is_array( func_get_arg( 2 ) ) ? func_get_arg( 2 ) : array();

		if ( $searchParameters === null ) {
			$searchParameters = array();
		}
		$searchParameters['q'] = $query;

		return $this->client->request(
			$this->context,
			'POST',
			'/intent',
			array(),
			$searchParameters,
			$this->context->readHostsArray,
			$this->context->connectTimeout,
			$this->context->searchTimeout,
			$requestHeaders
		);
	}

	/**
	 * Perform a search inside facets.
	 *
	 * @param $facetName
	 * @param $facetQuery
	 * @param array $searchParameters
	 * @param array $requestHeaders
	 *
	 * @return mixed
	 * @throws OlaSearchException
	 */
	public function searchForFacetValues( $facetName, $facetQuery, $searchParameters = array() ) {
		$requestHeaders = func_num_args() === 4 && is_array( func_get_arg( 3 ) ) ? func_get_arg( 3 ) : array();

		$searchParameters['facetQuery'] = $facetQuery;

		return $this->client->request(
			$this->context,
			'POST',
			'/connector/document',
			array(),
			array( 'params' => $this->client->buildQuery( $searchParameters ) ),
			$this->context->readHostsArray,
			$this->context->connectTimeout,
			$this->context->searchTimeout,
			$requestHeaders
		);
	}

	/**
	 * This function deletes all the indexed documents, mappings and taxonomy/entity terms.
	 *
	 * @return mixed
	 *
	 * @throws OlaSearchException
	 */
	public function wipeAll() {
		$requestHeaders = func_num_args() === 1 && is_array( func_get_arg( 0 ) ) ? func_get_arg( 0 ) : array();

		return $this->client->request(
			$this->context,
			'DELETE',
			'/connector/wipe',
			null,
			null,
			$this->context->writeHostsArray,
			$this->context->connectTimeout,
			$this->context->readTimeout,
			$requestHeaders
		);
	}

	/**
	 * @param string $name
	 * @param array $arguments
	 *
	 * @return mixed
	 */
	public function __call( $name, $arguments ) {
		if ( $name === 'browse' ) {
			if ( count( $arguments ) >= 1 && is_string( $arguments[0] ) ) {
				return call_user_func_array( array( $this, 'doBrowse' ), $arguments );
			}

			return call_user_func_array( array( $this, 'doBcBrowse' ), $arguments );
		}

		throw new \BadMethodCallException( sprintf( 'No method named %s was found.', $name ) );
	}
}
