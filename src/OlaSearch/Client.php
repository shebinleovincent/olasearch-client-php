<?php

/*
 * Copyright (c) 2018 Ola Search
 * http://www.olasearch.com/
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 *
 */

namespace OlaSearch;

/**
 * Entry point in the PHP API.
 * You should instantiate a Client object with your ProjectID, APIKey and Hosts
 * to start using Ola Search API.
 */
class Client {
	const CAINFO              = 'cainfo';
	const CURLOPT             = 'curloptions';
	const FAILING_HOSTS_CACHE = 'failingHostsCache';
	const OLA_SERVER          = 'server';
	const OLA_ENV             = 'env';

	/**
	 * @var ClientContext
	 */
	protected $context;

	/**
	 * @var array
	 */
	protected $curlConstants;

	/**
	 * @var array
	 */
	protected $curlOptions = array();

	/**
	 * Ola Search initialization.
	 *
	 * @param string $projectID the project ID you have in your admin interface
	 * @param string $apiKey a valid API key for the service
	 * @param array|null $hostsArray the list of hosts that you have received for the service
	 * @param array $options
	 *
	 * @throws \Exception
	 */
	public function __construct( $projectID, $apiKey, $hostsArray = null, $options = array() ) {
		if ( ! function_exists( 'curl_init' ) ) {
			throw new \Exception( 'OlaSearch requires the CURL PHP extension.' );
		}
		if ( ! function_exists( 'json_decode' ) ) {
			throw new \Exception( 'OlaSearch requires the JSON PHP extension.' );
		}

		$server = null;
		$env    = null;
		foreach ( $options as $option => $value ) {
			switch ( $option ) {
				case self::CURLOPT:
					$this->curlOptions = $this->checkCurlOptions( $value );
					break;
				case self::OLA_SERVER:
					$server = $value;
					break;
				case self::OLA_ENV:
					$env = $value;
					break;
				case self::FAILING_HOSTS_CACHE:
					if ( ! $value instanceof FailingHostsCache ) {
						throw new \InvalidArgumentException(
							'failingHostsCache must be an instance of \use OlaSearch\FailingHostsCache.'
						);
					}
					break;
				default:
					throw new \Exception( 'Unknown option: ' . $option );
			}
		}

		$failingHostsCache = isset( $options[self::FAILING_HOSTS_CACHE] ) ? $options[self::FAILING_HOSTS_CACHE] : null;
		$this->context     = new ClientContext(
			$server,
			$projectID,
			$env,
			$apiKey,
			$hostsArray,
			$failingHostsCache
		);
	}

	/**
	 * Release curl handle.
	 */
	public function __destruct() {
	}

	/**
	 * Change the default connect timeout of 1s to a custom value
	 * (only useful if your server has a very slow connectivity to Ola Search backend).
	 *
	 * @param int $connectTimeout the connection timeout
	 * @param int $timeout the read timeout for the query
	 * @param int $searchTimeout the read timeout used for search queries only
	 *
	 * @throws OlaSearchException
	 */
	public function setConnectTimeout( $connectTimeout, $timeout = 30, $searchTimeout = 5 ) {
		if ( $connectTimeout < 1 ) {
			$version = curl_version();

			if ( version_compare( $version['version'], '7.16.2', '<' ) ) {
				throw new OlaSearchException(
					"The timeout can't be a float with a curl version less than 7.16.2"
				);
			}
		}

		$this->context->connectTimeout = $connectTimeout;
		$this->context->readTimeout    = $timeout;
		$this->context->searchTimeout  = $searchTimeout;
	}

	/**
	 * The aggregation of the queries to retrieve the latest query
	 * uses the IP or the user token to work efficiently.
	 * If the queries are made from your backend server,
	 * the IP will be the same for all of the queries.
	 * We're supporting the following HTTP header to forward the IP of your end-user
	 * to the engine, you just need to set it for each query.
	 *
	 * @see https://www.olasearch.com/doc/faq/analytics/will-the-analytics-still-work-if-i-perform-the-search-through-my-backend
	 *
	 * @param string $ip
	 */
	public function setForwardedFor( $ip ) {
		$this->context->setForwardedFor( $ip );
	}

	/**
	 * It's possible to use the following token to track users that have the same IP
	 * or to track users that use different devices.
	 *
	 * @see https://www.olasearch.com/doc/faq/analytics/will-the-analytics-still-work-if-i-perform-the-search-through-my-backend
	 *
	 * @param string $token
	 */
	public function setOlaSearchUserToken( $token ) {
		$this->context->setOlaSearchUserToken( $token );
	}

	/**
	 * Call isAlive.
	 */
	public function isAlive() {
		return $this->request(
			$this->context,
			'GET',
			'/isalive',
			null,
			null,
			$this->context->readHostsArray,
			$this->context->connectTimeout,
			$this->context->readTimeout
		);
	}

	/**
	 * Allow to set custom headers.
	 *
	 * @param string $key
	 * @param string $value
	 */
	public function setExtraHeader( $key, $value ) {
		$this->context->setExtraHeader( $key, $value );
	}

	/**
	 * Get the index object initialized (no server call needed for initialization).
	 *
	 * @return Index
	 *
	 * @throws OlaSearchException
	 */
	public function initIndex() {
		return new Index( $this->context, $this );
	}

	/**
	 * @param array $args
	 *
	 * @return string
	 */
	public static function buildQuery( $args ) {
		foreach ( $args as $key => $value ) {
			if ( gettype( $value ) == 'array' ) {
				$args[$key] = Json::encode( $value );
			}
		}

		return http_build_query( $args );
	}

	/**
	 * @param ClientContext $context
	 * @param string $method
	 * @param string $path
	 * @param array $params
	 * @param array $data
	 * @param array $hostsArray
	 * @param int $connectTimeout
	 * @param int $readTimeout
	 * @param array $requestHeaders
	 *
	 * @return mixed
	 *
	 * @throws OlaSearchException
	 */
	public function request(
		$context,
		$method,
		$path,
		$params,
		$data,
		$hostsArray,
		$connectTimeout,
		$readTimeout
	) {
		$requestHeaders = func_num_args() === 9 && is_array( func_get_arg( 8 ) ) ? func_get_arg( 8 ) : array();

		$exceptions = array();
		$cnt        = 0;
		foreach ( $hostsArray as &$host ) {
			$cnt += 1;
			if ( $cnt == 3 ) {
				$connectTimeout += 2;
				$readTimeout    += 10;
			}
			try {
				$res = $this->doRequest( $context, $method, $host, $path, $params, $data, $connectTimeout, $readTimeout, $requestHeaders );
				if ( $res !== null ) {
					return $res;
				}
			} catch ( OlaSearchException $e ) {
				throw $e;
			} catch ( \Exception $e ) {
				$exceptions[$host] = $e->getMessage();
				if ( $context instanceof ClientContext ) {
					$context->addFailingHost( $host ); // Needs to be before the rotation otherwise it will not be rotated
					$context->rotateHosts();
				}
			}
		}
		throw new OlaSearchConnectionException( 'Hosts unreachable: ' . implode( ',', $exceptions ) );
	}

	/**
	 * @param ClientContext $context
	 * @param string $method
	 * @param string $host
	 * @param string $path
	 * @param array $params
	 * @param array $data
	 * @param int $connectTimeout
	 * @param int $readTimeout
	 * @param array $requestHeaders
	 *
	 * @return mixed
	 *
	 * @throws OlaSearchException
	 * @throws \Exception
	 */
	public function doRequest(
		$context,
		$method,
		$host,
		$path,
		$params,
		$data,
		$connectTimeout,
		$readTimeout
	) {
		$requestHeaders = func_num_args() === 9 && is_array( func_get_arg( 8 ) ) ? func_get_arg( 8 ) : array();

		if ( strpos( $host, 'http' ) === 0 ) {
			$url = $host . $path;
		} else {
			$url = 'https://' . $host . $path;
		}

		$params['connector'] = Version::getName();
		$params['projectId'] = $this->context->projectID;
		$params['env']       = $this->context->env;
		if ( $params != null && count( $params ) > 0 ) {
			$params2 = array();
			foreach ( $params as $key => $val ) {
				if ( is_array( $val ) ) {
					$params2[$key] = Json::encode( $val );
				} else {
					$params2[$key] = $val;
				}
			}
			$url .= '?' . http_build_query( $params2 );
		}

		// initialize curl library
		$curlHandle = curl_init();

		// set curl options
		try {
			foreach ( $this->curlOptions as $curlOption => $optionValue ) {
				curl_setopt( $curlHandle, constant( $curlOption ), $optionValue );
			}
		} catch ( \Exception $e ) {
			$this->invalidOptions( $this->curlOptions, $e->getMessage() );
		}

		//curl_setopt($curlHandle, CURLOPT_VERBOSE, true);

		$defaultHeaders = array(
			'X-OlaSearch-Project-Id' => $context->projectID,
			'X-OlaSearch-API-Key'    => $context->apiKey,
			'Authorization'          => $context->apiKey,
			'Content-type'           => 'application/json',
		);

		$headers = array_merge( $defaultHeaders, $context->headers, $requestHeaders );

		$curlHeaders = array();
		foreach ( $headers as $key => $value ) {
			$curlHeaders[] = $key . ': ' . $value;
		}

		curl_setopt( $curlHandle, CURLOPT_HTTPHEADER, $curlHeaders );

		curl_setopt( $curlHandle, CURLOPT_USERAGENT, Version::getUserAgent() );
		//Return the output instead of printing it
		curl_setopt( $curlHandle, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $curlHandle, CURLOPT_FAILONERROR, true );
		curl_setopt( $curlHandle, CURLOPT_ENCODING, '' );
		curl_setopt( $curlHandle, CURLOPT_SSL_VERIFYPEER, true );
		curl_setopt( $curlHandle, CURLOPT_SSL_VERIFYHOST, 2 );

		curl_setopt( $curlHandle, CURLOPT_URL, $url );
		$version = curl_version();
		if ( version_compare( phpversion(), '5.2.3', '>=' ) && version_compare( $version['version'], '7.16.2', '>=' ) && $connectTimeout < 1 ) {
			curl_setopt( $curlHandle, CURLOPT_CONNECTTIMEOUT_MS, $connectTimeout * 1000 );
			curl_setopt( $curlHandle, CURLOPT_TIMEOUT_MS, $readTimeout * 1000 );
		} else {
			curl_setopt( $curlHandle, CURLOPT_CONNECTTIMEOUT, $connectTimeout );
			curl_setopt( $curlHandle, CURLOPT_TIMEOUT, $readTimeout );
		}

		// The problem is that on (Li|U)nix, when libcurl uses the standard name resolver,
		// a SIGALRM is raised during name resolution which libcurl thinks is the timeout alarm.
		curl_setopt( $curlHandle, CURLOPT_NOSIGNAL, 1 );
		curl_setopt( $curlHandle, CURLOPT_FAILONERROR, false );

		if ( $method === 'GET' ) {
			curl_setopt( $curlHandle, CURLOPT_CUSTOMREQUEST, 'GET' );
			curl_setopt( $curlHandle, CURLOPT_HTTPGET, true );
			curl_setopt( $curlHandle, CURLOPT_POST, false );
		} else {
			if ( $method === 'POST' ) {
				$body = ( $data ) ? Json::encode( $data ) : '';
				curl_setopt( $curlHandle, CURLOPT_CUSTOMREQUEST, 'POST' );
				curl_setopt( $curlHandle, CURLOPT_POST, true );
				curl_setopt( $curlHandle, CURLOPT_POSTFIELDS, $body );
			} elseif ( $method === 'DELETE' ) {
				$body = ( $data ) ? Json::encode( $data ) : '';
				curl_setopt( $curlHandle, CURLOPT_CUSTOMREQUEST, 'DELETE' );
				curl_setopt( $curlHandle, CURLOPT_POSTFIELDS, $body );
				curl_setopt( $curlHandle, CURLOPT_POST, true );
			} elseif ( $method === 'PUT' ) {
				$body = ( $data ) ? Json::encode( $data ) : '';
				curl_setopt( $curlHandle, CURLOPT_CUSTOMREQUEST, 'PUT' );
				curl_setopt( $curlHandle, CURLOPT_POSTFIELDS, $body );
				curl_setopt( $curlHandle, CURLOPT_POST, true );
			}
		}
		$mhandle = $context->getMHandle( $curlHandle );

		// Do all the processing.
		$running = null;
		do {
			$mrc = curl_multi_exec( $mhandle, $running );
		} while ( $mrc == CURLM_CALL_MULTI_PERFORM );

		while ( $running && $mrc == CURLM_OK ) {
			if ( curl_multi_select( $mhandle, 0.1 ) == -1 ) {
				usleep( 100 );
			}
			do {
				$mrc = curl_multi_exec( $mhandle, $running );
			} while ( $mrc == CURLM_CALL_MULTI_PERFORM );
		}

		$http_status = (int)curl_getinfo( $curlHandle, CURLINFO_HTTP_CODE );
		$response    = curl_multi_getcontent( $curlHandle );
		$error       = curl_error( $curlHandle );

		if ( ! empty( $error ) ) {
			throw new \Exception( $error );
		}

		if ( $http_status === 0 || $http_status === 503 ) {
			// Could not reach host or service unavailable, try with another one if we have it
			$context->releaseMHandle( $curlHandle );
			curl_close( $curlHandle );

			return;
		}

		$context->releaseMHandle( $curlHandle );
		curl_close( $curlHandle );

		if ( $http_status == 204 ) {
			return '';
		}

		$answer = Json::decode( $response, true );

		if ( intval( $http_status / 100 ) == 4 ) {
			throw new OlaSearchException( isset( $answer['message'] ) ? $answer['message'] : $http_status . ' error', $http_status );
		} elseif ( intval( $http_status / 100 ) != 2 ) {
			throw new \Exception( $http_status . ': ' . $response, $http_status );
		}

		return $answer;
	}

	/**
	 * Checks if curl option passed are valid curl options.
	 *
	 * @param array $curlOptions must be array but no type required while first test throw clear Exception
	 *
	 * @return array
	 */
	protected function checkCurlOptions( $curlOptions ) {
		if ( ! is_array( $curlOptions ) ) {
			throw new \InvalidArgumentException(
				sprintf(
					'OlaSearch requires %s option to be array of valid curl options.',
					static::CURLOPT
				)
			);
		}

		$checkedCurlOptions = array_intersect( array_keys( $curlOptions ), array_keys( $this->getCurlConstants() ) );

		if ( count( $checkedCurlOptions ) !== count( $curlOptions ) ) {
			$this->invalidOptions( $curlOptions );
		}

		return $curlOptions;
	}

	/**
	 * Get all php curl available options.
	 *
	 * @return array
	 */
	protected function getCurlConstants() {
		if ( ! is_null( $this->curlConstants ) ) {
			return $this->curlConstants;
		}

		$curlAllConstants = get_defined_constants( true );

		if ( isset( $curlAllConstants['curl'] ) ) {
			$curlAllConstants = $curlAllConstants['curl'];
		} elseif ( isset( $curlAllConstants['Core'] ) ) { // hhvm
			$curlAllConstants = $curlAllConstants['Core'];
		} else {
			return $this->curlConstants;
		}

		$curlConstants = array();
		foreach ( $curlAllConstants as $constantName => $constantValue ) {
			if ( strpos( $constantName, 'CURLOPT' ) === 0 ) {
				$curlConstants[$constantName] = $constantValue;
			}
		}

		$this->curlConstants = $curlConstants;

		return $this->curlConstants;
	}

	/**
	 * throw clear Exception when bad curl option is set.
	 *
	 * @param array $curlOptions
	 * @param string $errorMsg add specific message for disambiguation
	 */
	protected function invalidOptions( array $curlOptions = array(), $errorMsg = '' ) {
		throw new \OutOfBoundsException(
			sprintf(
				'OlaSearch %s options keys are invalid. %s given. error message : %s',
				static::CURLOPT,
				Json::encode( $curlOptions ),
				$errorMsg
			)
		);
	}

	public function getContext() {
		return $this->context;
	}
}
