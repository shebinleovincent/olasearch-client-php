<?php

/*
 * Copyright (c) 2018 Ola Search
 * http://www.olasearch.com/
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 *
 */

namespace OlaSearch;

use Exception;

class ClientContext {
	const ENV_PRODUCTION = 'production';
	const ENV_STAGING    = 'staging';

	const SERVER_PRODUCTION = 'production';
	const SERVER_INTERNAL   = 'internal';
	const SERVER_LOCAL      = 'local';

	/**
	 * @var string
	 */
	public $server;

	/**
	 * @var string
	 */
	public $projectID;

	/**
	 * @var string
	 */
	public $env;

	/**
	 * @var string
	 */
	public $apiKey;

	/**
	 * @var array
	 */
	public $readHostsArray;

	/**
	 * @var array
	 */
	public $writeHostsArray;

	/**
	 * @var resource
	 */
	public $curlMHandle;

	/**
	 * @var string
	 */
	public $endUserIP;

	/**
	 * @var string
	 */
	public $olasearchUserToken;

	/**
	 * @var int
	 */
	public $connectTimeout;

	/**
	 * @var FailingHostsCache
	 */
	private $failingHostsCache;

	/**
	 * @param string|null $server
	 * @param string|null $projectID
	 * @param string|null $env
	 * @param string|null $apiKey
	 * @param array|null $hostsArray
	 * @param FailingHostsCache|null $failingHostsCache
	 *
	 * @throws Exception
	 */
	public function __construct( $server = null, $projectID = null, $env = null, $apiKey = null, $hostsArray = null, FailingHostsCache $failingHostsCache = null ) {
		// connect timeout of 2s by default
		$this->connectTimeout = 2;

		// global timeout of 30s by default
		$this->readTimeout = 30;

		// search timeout of 5s by default
		$this->searchTimeout = 5;

		$this->server    = $server;
		$this->projectID = $projectID;
		$this->env       = $env;
		$this->apiKey    = $apiKey;

		if ( ( $this->server == null || mb_strlen( $this->server ) == 0 ) ) {
			$this->server = self::SERVER_PRODUCTION;
		}
		if ( ( $this->env == null || mb_strlen( $this->env ) == 0 ) ) {
			$this->env = self::ENV_PRODUCTION;
		}

		$this->readHostsArray  = $hostsArray;
		$this->writeHostsArray = $hostsArray;

		if ( $this->readHostsArray == null || count( $this->readHostsArray ) == 0 ) {
			$this->readHostsArray  = $this->getDefaultReadHosts();
			$this->writeHostsArray = $this->getDefaultWriteHosts();
		}

		if ( $this->projectID == null || mb_strlen( $this->projectID ) == 0 ) {
			throw new Exception( 'OlaSearch requires an projectID.' );
		}

		if ( $this->apiKey == null || mb_strlen( $this->apiKey ) == 0 ) {
			throw new Exception( 'OlaSearch requires an apiKey.' );
		}

		$this->curlMHandle        = null;
		$this->endUserIP          = null;
		$this->olasearchUserToken = null;
		$this->headers            = array();

		if ( $failingHostsCache === null ) {
			$this->failingHostsCache = new InMemoryFailingHostsCache();
		} else {
			$this->failingHostsCache = $failingHostsCache;
		}

		$this->rotateHosts();
	}

	/**
	 *
	 * @return array
	 */
	private function getDefaultReadHosts() {
		if ( $this->server === self::SERVER_LOCAL ) {
			return array( 'http://localhost:3000' );
		}

		if ( $this->server === self::SERVER_INTERNAL ) {
			$hosts = array(
//				'api-staging-1.olasearch.com',
//				'api-staging-2.olasearch.com',
//				'api-staging-3.olasearch.com',
			);
			shuffle( $hosts );
			array_unshift( $hosts, 'api-staging.olasearch.com' );

			return $hosts;
		}

		$hosts = array(
//			'api-1.olasearch.com',
//			'api-2.olasearch.com',
//			'api-3.olasearch.com',
		);
		shuffle( $hosts );
		array_unshift( $hosts, 'api.olasearch.com' );

		return $hosts;
	}

	/**
	 *
	 * @return array
	 */
	private function getDefaultWriteHosts() {
		if ( $this->server === self::SERVER_LOCAL ) {

			return array( 'http://localhost:3333/api' );
		}

		if ( $this->server === self::SERVER_INTERNAL ) {
			$hosts = array(
//				'admin-staging-1.olasearch.com',
//				'admin-staging-2.olasearch.com',
//				'admin-staging-3.olasearch.com',
			);
			shuffle( $hosts );
			array_unshift( $hosts, 'admin-staging.olasearch.com/api' );

			return $hosts;
		}

		$hosts = array(
//			'admin-1.olasearch.com',
//			'admin-2.olasearch.com',
//			'admin-3.olasearch.com',
		);
		shuffle( $hosts );
		array_unshift( $hosts, 'admin.olasearch.com/api' );

		return $hosts;
	}

	/**
	 * Closes eventually opened curl handles.
	 */
	public function __destruct() {
		if ( is_resource( $this->curlMHandle ) ) {
			curl_multi_close( $this->curlMHandle );
		}
	}

	/**
	 * @param $curlHandle
	 *
	 * @return resource
	 */
	public function getMHandle( $curlHandle ) {
		if ( ! is_resource( $this->curlMHandle ) ) {
			$this->curlMHandle = curl_multi_init();
		}
		curl_multi_add_handle( $this->curlMHandle, $curlHandle );

		return $this->curlMHandle;
	}

	/**
	 * @param $curlHandle
	 */
	public function releaseMHandle( $curlHandle ) {
		curl_multi_remove_handle( $this->curlMHandle, $curlHandle );
	}

	/**
	 * @param string $ip
	 */
	public function setForwardedFor( $ip ) {
		$this->endUserIP = $ip;
	}

	/**
	 * @param string $token
	 */
	public function setOlasearchUserToken( $token ) {
		$this->olasearchUserToken = $token;
	}

	/**
	 * @param string $key
	 * @param string $value
	 */
	public function setExtraHeader( $key, $value ) {
		$this->headers[$key] = $value;
	}

	/**
	 * @param string $host
	 */
	public function addFailingHost( $host ) {
		$this->failingHostsCache->addFailingHost( $host );
	}

	/**
	 * @return FailingHostsCache
	 */
	public function getFailingHostsCache() {
		return $this->failingHostsCache;
	}

	/**
	 * This method is called to pass on failing hosts.
	 * If the host is first either in the failingHosts array, we
	 * rotate the array to ensure the next API call will be directly made with a working
	 * host. This mainly ensures we don't add the equivalent of the connection timeout value to each
	 * request to the API.
	 */
	public function rotateHosts() {
		$failingHosts = $this->failingHostsCache->getFailingHosts();
		$i            = 0;
		while ( $i <= count( $this->readHostsArray ) && in_array( $this->readHostsArray[0], $failingHosts ) ) {
			$i++;
			$this->readHostsArray[] = array_shift( $this->readHostsArray );
		}

		$i = 0;
		while ( $i <= count( $this->writeHostsArray ) && in_array( $this->writeHostsArray[0], $failingHosts ) ) {
			$i++;
			$this->writeHostsArray[] = array_shift( $this->writeHostsArray );
		}
	}
}
